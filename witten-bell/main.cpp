#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstdio>
#include <map>
#include <algorithm>
#include <cmath>
#include <string.h>
#include <codecvt>
#include <locale>

using namespace std;

struct mykey
{
    mykey(wchar_t data1, wchar_t data2)
    {
        c1 = data1;
        c2 = data2;
    }

    bool operator<(const mykey& other) const
    {
        if (c1 == other.c1)
        {
            return c2 < other.c2;
        }
        else
        {
            return c1 < other.c1;
        }
        return false;
    }

    wchar_t c1;
    wchar_t c2;
};

struct trigram
{
    trigram(wchar_t s1, wchar_t s2, wchar_t s3)
    {
        c1 = s1;
        c2 = s2;
        c3 = s3;
    }

    trigram(mykey key, wchar_t c)
    {
        c1 = key.c1;
        c2 = key.c2;
        c3 = c;
    }

    bool operator<(const trigram& other) const
    {
        if (c1 == other.c1)
        {
            if (c2 == other.c2)
            {
                return c3 < other.c3;
            }
            else
            {
                return c2 < other.c2;
            }
        }
        else
        {
            return c1 < other.c1;
        }
        return false;
    }

    wchar_t c1;
    wchar_t c2;
    wchar_t c3;

    mykey key()
    {
        return mykey{c1, c2};
    }
};

struct myfifo
{
    wchar_t data[3];

    void push(wchar_t c)
    {
        data[0] = data[1];
        data[1] = data[2];
        data[2] = c;
    }

    mykey key()
    {
        return mykey{data[0], data[1]};
    }
};

wchar_t wchar_to_lower(const wchar_t& w)
{
    wchar_t c = 0;
    switch (w)
    {
    case 282:
    case 283:
        c = 283;
        break;
    case 352:
    case 353:
        c = 353;
        break;
    case 268:
    case 269:
        c = 269;
        break;
    case 344:
    case 345:
        c = 345;
        break;
    case 381:
    case 382:
        c = 382;
        break;
    case 221:
    case 253:
        c = 253;
        break;
    case 193:
    case 225:
        c = 225;
        break;
    case 205:
    case 237:
        c = 237;
        break;
    case 201:
    case 233:
        c = 233;
        break;
    case 218:
    case 250:
        c = 250;
        break;
    case 366:
    case 367:
        c = 367;
        break;
    case 356:
    case 357:
        c = 357;
        break;
    case 211:
    case 243:
        c = 243;
        break;
    case 270:
    case 271:
        c = 271;
        break;
    case 327:
    case 328:
        c = 328;
        break;
    case 196:
    case 228:
        c = 228;
        break;
    case 313:
    case 314:
        c = 314;
        break;
    case 317:
    case 318:
        c = 318;
        break;
    case 212:
    case 244:
        c = 244;
        break;
    case 340:
    case 341:
        c = 341;
        break;
    }
    return c;
}

string wchar_decode(const wchar_t& w)
{
    string str = "";
    if (w <= 'z')
    {
        str += (char)w;
    }
    else
    {
        switch (w)
        {
        case 283:
            str += "ě";
            break;
        case 353:
            str += "š";
            break;
        case 269:
            str += "č";
            break;
        case 345:
            str += "ř";
            break;
        case 382:
            str += "ž";
            break;
        case 253:
            str += "ý";
            break;
        case 225:
            str += "á";
            break;
        case 237:
            str += "í";
            break;
        case 233:
            str += "é";
            break;
        case 250:
            str += "ú";
            break;
        case 367:
            str += "ů";
            break;
        case 357:
            str += "ť";
            break;
        case 243:
            str += "ó";
            break;
        case 271:
            str += "ď";
            break;
        case 328:
            str += "ň";
            break;
        case 228:
            str += "ä";
            break;
        case 314:
            str += "ĺ";
            break;
        case 318:
            str += "ľ";
            break;
        case 244:
            str += "ô";
            break;
        case 341:
            str += "ŕ";
            break;
        default:
            str += "";
        }
    }
    return str;
}

wchar_t remove_special(const wchar_t& w)
{
    wchar_t c = 0;

    switch (w)
    {
    case 283:
        c = 'e';
        break;
    case 353:
        c = 's';
        break;
    case 269:
        c = 'c';
        break;
    case 345:
        c = 'r';
        break;
    case 382:
        c = 'z';
        break;
    case 253:
        c = 'y';
        break;
    case 225:
        c = 'a';
        break;
    case 237:
        c = 'i';
        break;
    case 233:
        c = 'e';
        break;
    case 250:
        c = 'u';
        break;
    case 367:
        c = 'u';
        break;
    case 357:
        c = 't';
        break;
    case 243:
        c = 'o';
        break;
    case 271:
        c = 'd';
        break;
    case 328:
        c = 'n';
        break;
    case 228:
        c = 'a';
        break;
    case 314:
        c = 'l';
        break;
    case 318:
        c = 'l';
        break;
    case 244:
        c = 'o';
        break;
    case 341:
        c = 'r';
        break;
    default:
        c = w;
    }

    return c;
}

using Converter = std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>;

int main(int argc, char ** argv)
{
    int n = 3;
    string filepath;
    if (argc == 1)
    {
        cout << "Enter filepath: (enter to continue)" << endl;
        cin >> filepath;
    }
    else
    {
        if (argc < 3)
        {
            cout << "Usage: ./<program_name><path_to_file>" << endl;
            return -1;
        }

        filepath = argv[2];
    }

    map<mykey, double> key_cnts;
    map<trigram, double> trigram_cnts;
    map<wchar_t, double> unique_cnts;

    myfifo fifo;
    fifo.push(wchar_t{});
    fifo.push(wchar_t{});

    bool exchange = strcmp(argv[1], "nospec") == 0;

    /* Form n-gram dataset */
    ifstream in(filepath);
    if (in.is_open())
    {
        string line;
        while (getline(in, line))
        {
            transform(line.begin(), line.end(), line.begin(), ::tolower);
            wstring cl = Converter{}.from_bytes(line);
            for (auto c : cl)
            {
                if (c > 'z')
                {
                    c = wchar_to_lower(c);
                }
                if (c == 0)
                {
                    continue;
                }

                if (exchange)
                {
                    c = remove_special(c);
                }

                unique_cnts[c]++;
                fifo.push(c);
                key_cnts[fifo.key()]++;
                trigram_cnts[trigram{fifo.data[0], fifo.data[1], fifo.data[2]}]++;
            }
        }
        in.close();
    }
    else
    {
        cout << "Cannot open file " << filepath << endl;
        return -1;
    }

    /* Write to file */
    string infilename = filepath.substr(0, filepath.find_last_of('.'));
    string outname = infilename;
    outname += "_" + to_string(n) + "-gram.lang";

    /* Witten-Bell */
    vector<pair<trigram, double>> lang;

    map<mykey, double> total;
    map<mykey, double> unique_key_cnts;
    for (auto it : trigram_cnts)
    {
        trigram t = it.first;
        unique_key_cnts[t.key()]++;
        total[t.key()] += it.second;
    }

    for (auto it : trigram_cnts)
    {
        trigram t = it.first;
        double T = unique_key_cnts[t.key()];
        double N = total[t.key()];

        double pwb = it.second / (N + T);
        pair<trigram, double> p{t, pwb};
        lang.push_back(p);
    }

    double comb = pow(unique_cnts.size(), n);

    for (auto c1 : unique_cnts)
    {
        for (auto c2 : unique_cnts)
        {
            mykey key{c1.first, c2.first};
            double T = unique_key_cnts[key];

            trigram t{key, wchar_t{}};

            double Z = comb - T;
            double N = total[key];
            double pwb = T / (Z * (N + T));
            pair<trigram, double> p{t, pwb};
            lang.push_back(p);
        }
    }

//    /* Sort */
//    sort(begin(lang), end(lang),
//         [](const pair<trigram, double>& a,
//            const pair<trigram, double>& b)
//    {
//        return a.second > b.second;
//    });

    lang.erase(lang.begin());
    lang.erase(lang.begin());

    ofstream out(outname);
    if (out.is_open(), ios::binary)
    {
        for (const auto& it : lang)
        {
            trigram t = it.first;
            out << wchar_decode(t.c1) << ";"
                << wchar_decode(t.c2) << ";"
                << wchar_decode(t.c3) << ";"
                << (isnan(it.second)?0:it.second) << ";" << endl;
        }
        out.close();
    }
    else
    {
        cout << "Cannot write to file outname" << endl;
        return -1;
    }

    return 0;
}
