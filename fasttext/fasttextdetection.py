import fasttext

spec = True

lm = fasttext.load_model('lid.176.bin')

if spec:
    files = ['cz01.cs', 'cz02.cs', 'cz03.cs', 'cz04.cs', 'cz05.cs', 'cz06.cs', 'cz07.cs', 'sk01.sk', 'sk02.sk', 'sk03.sk', 'sk04.sk', 'sk05.sk', 'sk06.sk', 'sk07.sk']
else:
    files = ['cz01.cs.m', 'cz02.cs.m', 'cz03.cs.m', 'cz04.cs.m', 'cz05.cs.m', 'cz06.cs.m', 'cz07.cs.m', 'sk01.sk.m', 'sk02.sk.m', 'sk03.sk.m', 'sk04.sk.m', 'sk05.sk.m', 'sk06.sk.m', 'sk07.sk.m']

for file in files:
    with open(file, "r", encoding='utf-8') as f:
        text = f.read()

    text = text.replace('\n', ' ')
    text = text.lower()

    prediction = lm.predict(text, k=1)

    if prediction[0][0] == '__label__cs':
        language = 'cestina'
    elif prediction[0][0] == '__label__sk':
        language = 'slovenstine'
    else:
        language = prediction[0][0]

    print(file + ' je v jazyce ' + language)
