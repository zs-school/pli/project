#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstdio>
#include <map>
#include <algorithm>
#include <cmath>
#include <string.h>
#include <codecvt>
#include <locale>
#include <sstream>

using namespace std;

struct mykey
{
    mykey(wchar_t data1, wchar_t data2)
    {
        c1 = data1;
        c2 = data2;
    }

    bool operator<(const mykey& other) const
    {
        if (c1 == other.c1)
        {
            return c2 < other.c2;
        }
        else
        {
            return c1 < other.c1;
        }
        return false;
    }

    wchar_t c1;
    wchar_t c2;
};

struct trigram
{
    trigram(){}
    trigram(wchar_t s1, wchar_t s2, wchar_t s3)
    {
        c1 = s1;
        c2 = s2;
        c3 = s3;
    }

    bool operator<(const trigram& other) const
    {
        if (c1 == other.c1)
        {
            if (c2 == other.c2)
            {
                return c3 < other.c3;
            }
            else
            {
                return c2 < other.c2;
            }
        }
        else
        {
            return c1 < other.c1;
        }
        return false;
    }

    wchar_t c1;
    wchar_t c2;
    wchar_t c3;

    mykey key()
    {
        return mykey{c1, c2};
    }
};

struct myfifo
{
    wchar_t data[3];

    trigram push(wchar_t c)
    {
        data[0] = data[1];
        data[1] = data[2];
        data[2] = c;
        trigram t;
        t.c1 = data[0];
        t.c2 = data[1];
        t.c3 = data[2];
        return t;
    }

    mykey key()
    {
        return mykey{data[0], data[1]};
    }
};

using Converter = std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>;

size_t split(const std::string &txt, std::vector<std::string> &strs, char ch)
{
    size_t pos = txt.find( ch );
    size_t initialPos = 0;
    strs.clear();

    // Decompose statement
    while( pos != std::string::npos ) {
        strs.push_back( txt.substr( initialPos, pos - initialPos ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, std::min( pos, txt.size() ) - initialPos + 1 ) );

    return strs.size();
}

void load_lang(const string& path, map<trigram, double>& lang)
{
    ifstream in;
    in.open(path);
    if (in.is_open())
    {
        string line;
        while (getline(in, line))
        {
            trigram t;
            wstring wline = Converter{}.from_bytes(line);
            wstringstream ss{wline};

            wstring block;
            wchar_t delim = ';';
            getline(ss, block, delim);
            t.c1 = block[0];
            block.clear();
            getline(ss, block, delim);
            t.c2 = block[0];
            block.clear();
            getline(ss, block, delim);
            t.c3 = block[0];
            block.clear();
            getline(ss, block, delim);
            double value = stod(block.c_str());
            lang[t] = value;
        }
    }
}

wchar_t wchar_to_lower(const wchar_t& w)
{
    wchar_t c = 0;
    switch (w)
    {
    case 282:
    case 283:
        c = 283;
        break;
    case 352:
    case 353:
        c = 353;
        break;
    case 268:
    case 269:
        c = 269;
        break;
    case 344:
    case 345:
        c = 345;
        break;
    case 381:
    case 382:
        c = 382;
        break;
    case 221:
    case 253:
        c = 253;
        break;
    case 193:
    case 225:
        c = 225;
        break;
    case 205:
    case 237:
        c = 237;
        break;
    case 201:
    case 233:
        c = 233;
        break;
    case 218:
    case 250:
        c = 250;
        break;
    case 366:
    case 367:
        c = 367;
        break;
    case 356:
    case 357:
        c = 357;
        break;
    case 211:
    case 243:
        c = 243;
        break;
    case 270:
    case 271:
        c = 271;
        break;
    case 327:
    case 328:
        c = 328;
        break;
    case 196:
    case 228:
        c = 228;
        break;
    case 313:
    case 314:
        c = 314;
        break;
    case 317:
    case 318:
        c = 318;
        break;
    case 212:
    case 244:
        c = 244;
        break;
    case 340:
    case 341:
        c = 341;
        break;
    }
    return c;
}

double get_probability(map<trigram, double>& m, const trigram& t)
{
    double p = 0;
    trigram tri = t;
    if (m.count(tri))
    {
        p = m[tri];
    }
    else
    {
        tri.c3 = 0;
        if (m.count(tri))
        {
            p = m[tri];
        }
    }
    return p;
}

wchar_t remove_special(const wchar_t& w)
{
    wchar_t c = 0;

    switch (w)
    {
    case 283:
        c = 'e';
        break;
    case 353:
        c = 's';
        break;
    case 269:
        c = 'c';
        break;
    case 345:
        c = 'r';
        break;
    case 382:
        c = 'z';
        break;
    case 253:
        c = 'y';
        break;
    case 225:
        c = 'a';
        break;
    case 237:
        c = 'i';
        break;
    case 233:
        c = 'e';
        break;
    case 250:
        c = 'u';
        break;
    case 367:
        c = 'u';
        break;
    case 357:
        c = 't';
        break;
    case 243:
        c = 'o';
        break;
    case 271:
        c = 'd';
        break;
    case 328:
        c = 'n';
        break;
    case 228:
        c = 'a';
        break;
    case 314:
        c = 'l';
        break;
    case 318:
        c = 'l';
        break;
    case 244:
        c = 'o';
        break;
    case 341:
        c = 'r';
        break;
    default:
        c = w;
    }

    return c;
}

int main(int argc, char ** argv)
{
    if (argc < 4)
    {
        return -1;
    }
    /* Load language models */
    map<trigram, double> cz;
    map<trigram, double> sk;

    myfifo fifo;

    string spec = argv[1];
    string spec2 = argv[2];

    bool nospec_lang = false;
    bool nospec_input = false;

    vector<string> prs;
    split(spec, prs, '=');

    if (prs[0] == "lang")
    {
        if (prs[1] == "nospec")
        {
            nospec_lang = true;
        }
    }

    if (prs[0] == "nospec")
    {
        nospec_input = true;
    }

    prs.clear();
    split(spec2, prs, '=');

    if (prs[0] == "lang")
    {
        if (prs[1] == "nospec")
        {
            nospec_lang = true;
        }
    }

    if (prs[0] == "nospec")
    {
        nospec_input = true;
    }

    string cs_path = "cs.lang.";
    string sk_path = "sk.lang.";
    string ext = "spec";
    if (nospec_lang)
    {
        ext = "nospec";
    }
    cs_path += ext;
    sk_path += ext;
    load_lang(cs_path, cz);
    load_lang(sk_path, sk);

    double cz_match = 0;
    double sk_match = 0;
    double cz_det = 0;
    double sk_det = 0;
    double cz_files = 0;
    double sk_files = 0;


    for (int fn = 2; fn < argc; fn++)
    {
        double czp = 0;
        double skp = 0;

        double cnt = 0;

        double czn = 0;
        double skn = 0;

        string path = argv[fn];
        vector<string> ltv;
        split(path, ltv, '.');


        ifstream in(path);
        if (in.is_open())
        {
            if (ltv[1] == "cs")
            {
                cz_files++;
            }
            else if (ltv[1] == "sk")
            {
                sk_files++;
            }

            string line;
            while (getline(in, line))
            {
                transform(line.begin(), line.end(), line.begin(), ::tolower);
                wstring wline = Converter{}.from_bytes(line);
                for (auto c : wline)
                {
                    if (c > 'z')
                    {
                        c = wchar_to_lower(c);
                    }
                    if (c == 0)
                    {
                        continue;
                    }

                    if (nospec_input)
                    {
                        c = remove_special(c);
                    }

                    trigram t = fifo.push(c);
                    double ptr_cz = get_probability(cz, t);
                    double ptr_sk = get_probability(sk, t);

                    cnt++;
                    if (ptr_cz > 0)
                        czp *= ptr_cz;
                    if (ptr_sk > 0)
                        skp *= ptr_sk;

                    if (ptr_cz > ptr_sk)
                    {
                        czn++;
                    }

                    if (ptr_sk > ptr_cz)
                    {
                        skn++;
                    }
                }
            }
        }

        if (czn > skn)
        {
            cz_det++;
            if (ltv[1] == "cs")
            {
                cz_match++;
            }
        }

        if (skn > czn)
        {
            sk_det++;
            if (ltv[1] == "sk")
            {
                sk_match++;
            }
        }
    }

    printf("input CZ files: %d, CZ detected files: %d, matched: %d, success rate: %5.3f\r\n", (int)cz_files, (int)cz_det, (int)cz_match, ((cz_match / cz_files) * ((cz_match + sk_match) / (cz_files + sk_files))) * 100);
    printf("input SK files: %d, SK detected files: %d, matched: %d, success rate: %5.3f\r\n", (int)sk_files, (int)sk_det, (int)sk_match, ((sk_match / sk_files) * ((cz_match + sk_match) / (cz_files + sk_files))) * 100);

    return 0;
}
