### Korpusy

Jako korpusy jsem zvolil wikipedii v textové formě - českou a slovenskou.

### Úprava veškerých vstupních dat

Všechny korpusy jsem projel skiptem na odstranění znaků, které nepatří do abeced jazyků.

`sed 's/[^a-zA-ZěščřžýáíéúůĚŠČŘŽÝÁÍÉÚŮďĎťŤňŇÄäĹĺĽľÔôŔŕ]//g' korpus >> korpus.data`

### Tvorba slovníků (Witten-Bell)

Ve složkách witten-bell se vytváří jazykové modely.

Vytvoření jazykového modelu `./witten-bell <"spec"|"nospec"> <korpus>` trigramy po znacích. Výstup je textový soubor, snadno čitelný. Trigramy, které nejsou v korpusu, aby byly upraveny přes witten-bell, tak mají třetí znak prázdný a mají stejnou pravděpodobnost pro jakýkoliv 3 znak z abeced.

### Testování Bayes

Použití jazykových modelů k rozpoznání textu vyžaduje mít `cs.lang.spec`, `cs.lang.nospec`, `sk.lang.spec` a `sk.lang.nospec` ve stejné složce jako je binárka `bayes`. Test textu `./bayes lang=<"spec"|"nospec"> <"spec"|"nospec"> cs01.cs`. Každý další parametr je vstupní soubor.
První dva parametry nastavují volbu slovníku a volbu zpracování dat. Jeden z prvních dvou parametrů musí být `lang=nospec` nebo `lang=spec` a druhý `spec` nebo `nospec`. Parametr s `lang=` provádí výběr slovníku. Parametr bez `lang=` nastavuje úpravu vstupních dat (odstraňuje diakritiku).

Odstranění diakritiky je funkcí `remove_special(wchar_t)`, která přepíše znak na jeho verzi bez diakritiky.
Všechny znaky se převádí do malých znaků (lowercase);

Všechny vstupní soubory vyžadují být předzpracovány, aby obsahovaly pouze znaky abeced daných jazyků a nové řádky. Nesmí obsahovat jiné znaky (nebo alespoň nesmí mít ascii hodnotu nižší než 127). Viz zmíněná *úprava veškerých vstupních dat*.

Při rozpoznávání se nejprve kontroluje shoda trigramů, pokud není nalezena tak se z testovaného trigramu odstraní 3. znak a testuje se znovu (witten-bell).

Testování s diakritikou a bez diakritiky je zvolením prvního parametru `spec` nebo `nospec`. `nospec = bez diakritiky`

Přiložené testovací texty na slovnících s diakritikou
```
/* Vstup s diakritikou */
./bayes lang=spec spec cz01.cs cz02.cs cz03.cs cz04.cs cz05.cs cz06.cs cz07.cs sk01.sk sk02.sk sk03.sk sk04.sk sk05.sk sk06.sk sk07.sk 
input CZ files: 7, CZ detected files: 7, matched: 7, success rate: 100.000
input SK files: 7, SK detected files: 7, matched: 7, success rate: 100.000

/* Vstup bez diakritiky */
./bayes lang=spec nospec cz01.cs cz02.cs cz03.cs cz04.cs cz05.cs cz06.cs cz07.cs sk01.sk sk02.sk sk03.sk sk04.sk sk05.sk sk06.sk sk07.sk 
input CZ files: 7, CZ detected files: 5, matched: 4, success rate: 40.816
input SK files: 7, SK detected files: 8, matched: 6, success rate: 61.224
```

Slovníky bez diakritiky
```
/* Vstup s diakritikou */
./bayes lang=nospec spec cz01.cs cz02.cs cz03.cs cz04.cs cz05.cs cz06.cs cz07.cs sk01.sk sk02.sk sk03.sk sk04.sk sk05.sk sk06.sk sk07.sk 
input CZ files: 7, CZ detected files: 7, matched: 7, success rate: 100.000
input SK files: 7, SK detected files: 7, matched: 7, success rate: 100.000

/* Vstup bez diakritiky */
./bayes lang=nospec nospec cz01.cs cz02.cs cz03.cs cz04.cs cz05.cs cz06.cs cz07.cs sk01.sk sk02.sk sk03.sk sk04.sk sk05.sk sk06.sk sk07.sk 
input CZ files: 7, CZ detected files: 7, matched: 7, success rate: 100.000
input SK files: 7, SK detected files: 7, matched: 7, success rate: 100.000
```

Jelikož se při testování textů se speciálními charaktery na slovnících, které speciální znaky neobsahují, kde se poté speciální znaky zahodí, tak dostame stejné výsledky.

### Kroskompilace na Windows

Není možná kvůli rozdílnému kódování znaků mezi systémy Linux a Windows.

### Fasttext

Test textů s diakritikou:
```
cz01.cs je CZ
cz02.cs je CZ
cz03.cs je CZ
cz04.cs je CZ
cz05.cs je CZ
cz06.cs je CZ
cz07.cs je CZ
sk01.sk je SK
sk02.sk je SK
sk03.sk je SK
sk04.sk je SK
sk05.sk je SK
sk06.sk je SK
sk07.sk je CZ
```

Test textů bez diakritiky:
```
cz01.cs.m je CZ
cz02.cs.m je CZ
cz03.cs.m je DE
cz04.cs.m je CZ
cz05.cs.m je CZ
cz06.cs.m je CZ
cz07.cs.m je CZ
sk01.sk.m je SK
sk02.sk.m je SK
sk03.sk.m je SK
sk04.sk.m je SV
sk05.sk.m je SK
sk06.sk.m je SK
sk07.sk.m je SL
```
